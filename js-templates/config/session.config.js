import session from 'express-session';


function getSession(){
    const oneDay = 1000 * 60 * 60 * 24;

    const sessionOptions = {
        secret: "thisismysecrctekeyfhrgfgrfrty84fwir767", //RANDOM STRING NA AUTHENTIKACIU SESSION
        name: "network-log", //NAZOV SESSION
        saveUninitialized: true, //POVOLUJE NEINICIALIZOVANEJ SESSION POSLANIE DO SESSION STORE
        cookie: {
            httpOnly: true, //COOKIE BUDE POSIELANIE LEN CEZ HTTP A NEBUDE DOSTUPNE CEZ KLIENTA
            sameSite: true, //NASTAVI COOKIE LEN NA TUTO ROVNAKU STRANKU
            maxAge: oneDay, //DLZKA COOKIE PRE SESSION OD Date.now()
        },
        resave: false //POVOLUJES UKLADAT SESSION SPAT DO SESSION STORE AJ KED NEBOLA NIKDY MODIFIKOVANA POCAS REQUESTU
    }

    return session(sessionOptions);
}


export default getSession;