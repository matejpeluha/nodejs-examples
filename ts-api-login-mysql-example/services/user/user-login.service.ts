import bcrypt from 'bcrypt';
import {MysqlError} from 'mysql';

import {Request, Response} from "express";
import {LoginSession} from "../../models/interfaces/sessions/LoginSession";
import {SimpleAnswerJson} from "../../models/interfaces/jsons/SimpleAnswerJson";
import {ApiRequestService} from "../abstract/api-request.service";
import {OneUserJson} from "../../models/interfaces/jsons/user/OneUserJson";
import {OneFullProfileJson} from "../../models/interfaces/jsons/user/OneFullProfileJson";

class UserLoginService extends ApiRequestService{
    private readonly mail: string;
    private readonly password: string;
    private profileId: number | undefined;
    private session: LoginSession;

    constructor(req: Request, res: Response) {
        super(res);
        this.mail = req.body.mail;
        this.password = req.body.password;
        this.session = (req.session as LoginSession);
    }

    public loginUser(): void{
        const queryString: string = "SELECT * FROM profiles WHERE mail=?";
        const values: string[] = [this.mail];

        this.connection.query(queryString, values, this.handleUserLogin);
    }

    private handleUserLogin = (err: MysqlError | null | boolean, results: OneFullProfileJson[]): void => {
        if (err){
            this.res.status(500).send();
        }
        else if (results.length < 1){
            this.res.status(409).send("Profile does not exist");
        }
        else{
            this.profileId = results[0].ID;
            const savedPassword: string = results[0].password;
            bcrypt.compare(this.password, savedPassword, this.handleLogin);
        }
    }

    private handleLogin = (err: Error | undefined, result: boolean | undefined): void => {
        if (err){
            this.res.sendStatus(500);
        }
        else if (!result){
            this.res.status(403).send("Wrong password");
        }
        else{
            this.session.profileId = this.profileId;
            this.res.status(200).send("PRIHLASENY");
        }
        this.res.end();
    }

    public logoutUser(): void{
        this.session.destroy(() => {
            this.res.status(200).send("ODHLASENY");
        });
    }

    public showLoggedInfo(): void{
        if (this.session.profileId){
            this.showLoggedName();
        }
        else{
            const jsonObject: SimpleAnswerJson = {answer: "NOT LOGGED"};
            this.res.json(jsonObject);
        }
    }

    private showLoggedName(): void{
        const queryString: string = "SELECT users.first_name, users.last_name, users.ID FROM users " +
            "JOIN profiles ON users.ID=profiles.user_id " +
            "WHERE profiles.ID=?";
        const values: (number | undefined)[] = [this.session.profileId];

        this.connection.query(queryString, values, this.handleShowLoggedName);
    }

    private handleShowLoggedName = (err: MysqlError | null | boolean, results: OneUserJson[]): void =>{
        if (err){
            this.res.sendStatus(500);
        }
        else{
            const userName: string = results[0].first_name + " " + results[0].last_name;
            const jsonObject: SimpleAnswerJson = {answer: userName + " Logged"};
            this.res.json(jsonObject);
        }
    }
}

export default UserLoginService;