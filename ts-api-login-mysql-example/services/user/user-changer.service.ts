import {Request, Response} from "express";
import {MysqlError} from "mysql";
import {ApiRequestService} from "../abstract/api-request.service";
import {SimpleAnswerJson} from "../../models/interfaces/jsons/SimpleAnswerJson";
import {ChangeResultMysqlJson} from "../../models/interfaces/jsons/ChangeResultMysqlJson";

class UserChangerService extends ApiRequestService{

    constructor(req: Request, res: Response) {
        super(res);
    }

    public deleterUser(userId: string): void{
        const queryString: string = "DELETE FROM users WHERE users.ID=?";
        const values: string[] = [userId];

        this.connection.query(queryString, values, this.handleDeleteUser);
    }

    private handleDeleteUser = (err: MysqlError | null, result: ChangeResultMysqlJson): void => {
        if (err){
            this.res.sendStatus(500);
        }
        else if (result.affectedRows < 1){
            this.res.status(404).send("User was not deleted, user does not exist");
        }
        else{
            const jsonObject: SimpleAnswerJson = {answer: "User deleted"};
            this.res.json(jsonObject);
        }
    }

    public changeMail(userId: string, mail: string): void{
        const queryString: string = "UPDATE profiles SET mail=? WHERE user_id=?";
        const values: string[] = [mail, userId];

        this.connection.query(queryString, values, this.handleChangeMail);
    }

    private handleChangeMail = (err: MysqlError | null, result: ChangeResultMysqlJson): void => {
        if (err){
            this.res.sendStatus(500);
        }
        else if (result.affectedRows < 1){
            this.res.status(404).send("User does not exist");
        }
        else{
            const jsonObject: SimpleAnswerJson = {answer: "Mail updated"};
            this.res.json(jsonObject);
        }
        this.res.end();
    }
}

export {UserChangerService};