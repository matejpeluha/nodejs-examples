import bcrypt from 'bcrypt';
import {MysqlError} from 'mysql';
import {Request, Response} from "express";

import {ApiRequestService} from "../abstract/api-request.service";
import {ChangeResultMysqlJson} from "../../models/interfaces/jsons/ChangeResultMysqlJson";




class UserRegistrationService extends ApiRequestService{
    private readonly password: string;
    private readonly mail: string;
    private readonly firstName: string;
    private readonly lastName: string;
    private userId: number | undefined;


    constructor(req: Request, res: Response) {
        super(res);
        this.password = req.body.password;
        this.mail = req.body.mail;
        this.firstName = req.body.first_name;
        this.lastName = req.body.last_name;
    }

    public registerUser(): void{
        const queryString: string = "SELECT ID FROM profiles WHERE mail=?";
        const values: string[] = [this.mail];

        this.connection.query(queryString, values, this.handleUserRegistration);
    }

    private handleUserRegistration = (err: MysqlError | null | boolean, results: {ID: number}[]): void => {
        if (err){
            this.res.sendStatus(500);
        }
        else if (results.length > 0){
            this.res.status(409).send("Mail already used");
        }
        else{
            this.startUserRegistration();
        }
    }

    private startUserRegistration(): void{
        const queryString: string = "INSERT INTO users (first_name, last_name) VALUES (?, ?)";
        const values: string[] = [this.firstName, this.lastName];

        this.connection.query(queryString, values, this.finishUserRegistration);
    }

    private finishUserRegistration = (err: MysqlError | null | boolean, result: ChangeResultMysqlJson): void => {
        if (err){
            this.res.sendStatus(500);
            this.res.end();
        }
        else{
            this.handleProfileRegistration(result);
        }
    }

    private handleProfileRegistration(result: ChangeResultMysqlJson): void{
        this.userId = result.insertId;
        const saltRounds: number = 10;
        bcrypt.hash(this.password, saltRounds, this.registerProfile);
    }

    private registerProfile = (err: Error | undefined, hashedPassword: string) => {
        const queryString: string = "INSERT INTO profiles (user_id, mail, password) VALUES (?, ?, ?)";
        const values: (number | string | undefined )[] = [this.userId, this.mail, hashedPassword];

        this.connection.query(queryString, values, this.finishProfileRegistration);
    }

    private finishProfileRegistration = (err: MysqlError | null | undefined, result: ChangeResultMysqlJson): void => {
        if (err){
            this.res.sendStatus(500);
        }
        else{
            this.res.sendStatus(200);
        }

        this.res.end();
    }
}

export default UserRegistrationService;