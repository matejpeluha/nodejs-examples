import {Request, Response} from "express";
import {MysqlError} from 'mysql';
import {AllUserProfilesJson} from "../../models/interfaces/jsons/user/AllUserProfilesJson";
import {ApiRequestService} from "../abstract/api-request.service";
import {OneUserProfileJson} from "../../models/interfaces/jsons/user/OneUserProfileJson";

class UserFinderService extends ApiRequestService{

    constructor(req: Request, res: Response) {
        super(res);
    }

    public getAllUsers(): void{
        const queryString: string = "SELECT users.first_name, users.last_name, users.ID as userId, profiles.mail " +
                                    "FROM users JOIN profiles On users.ID=profiles.user_id";
        const values: undefined[] = [];

        this.connection.query(queryString, values, this.handleGetAllUsers);
    }

    private handleGetAllUsers = (err: MysqlError | null, results: OneUserProfileJson[]): void => {
        if (err){
            this.res.sendStatus(500);
        }
        else{
            const jsonObject: AllUserProfilesJson = {allUsers: results};
            this.res.json(jsonObject);
        }

        this.res.end();
    }

    public getUser(userId: string): void{
        const queryString: string = "SELECT users.first_name, users.last_name, users.ID as userId, profiles.mail " +
            "FROM users JOIN profiles On users.ID=profiles.user_id WHERE users.ID=? LIMIT 1";
        const values: string[] = [userId];

        this.connection.query(queryString, values, this.handleGetUser);
    }

    private handleGetUser = (err: MysqlError | null, results: OneUserProfileJson[]): void => {
        if (err){
            this.res.sendStatus(500);
        }
        else if (results.length < 1){
            this.res.status(404).send("User does not exist");
        }
        else{
            const user: OneUserProfileJson = results[0];
            this.res.json(user);
        }

        this.res.end();
    }

    public getUserMail(userId: string): void{
        const queryString: string = "SELECT profiles.mail FROM profiles WHERE profiles.user_id=? LIMIT 1";
        const values: string[] = [userId];

        this.connection.query(queryString, values, this.handleGetUserMail);
    }

    private handleGetUserMail = (err: MysqlError | null, results: {mail: string}[]): void => {
        if (err){
            this.res.json(500);
        }
        else if (results.length < 1){
            this.res.status(404).send("User does not exist");
        }
        else{
            const jsonObject: {mail: string} = results[0];
            this.res.json(jsonObject);
        }
    }
}

export {UserFinderService};