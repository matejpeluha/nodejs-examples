import {RequestHandler} from "express";
import session from "express-session";
import {CookieSessionOptions} from "../models/interfaces/options/CookieSessionOptions";
import {SessionOptions} from "../models/interfaces/options/SessionOptions";


const oneDay: number = 1000 * 60 * 60 * 24;

const cookieSessionOptions: CookieSessionOptions = {
    httpOnly: true,
    sameSite: true,
    maxAge: oneDay,
};

const sessionOptions: SessionOptions = {
    secret: "thisismysecrctekeyfhrgfgrfrty84fwir767",
    name: "network-log",
    saveUninitialized: true,
    cookie: cookieSessionOptions,
    resave: false
};

const loginSession: RequestHandler = session(sessionOptions);

export {loginSession};

