import {Connection, createConnection} from "mysql";
import {ConnectionOptions} from "../models/interfaces/options/ConnectionOptions";


const connectionOptions: ConnectionOptions = {
    host: 'localhost',
    user: 'root',
    password: '',
    database: 'social_network'
};

const connection: Connection = createConnection(connectionOptions);


export {connection};