import {Request, Response} from "express";
import UserLoginService from "../../../services/user/user-login.service";

const postRequest = (req: Request, res: Response): void => {
    const userLoginService: UserLoginService = new UserLoginService(req, res);
    userLoginService.loginUser();
};

export {postRequest};