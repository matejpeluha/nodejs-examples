import {Request, Response} from "express";
import {UserFinderService} from "../../../services/user/user-finder.service";


const getParamRequest = (req: Request, res: Response): void => {
    const userId: string = req.params.userId;
    const userFinderService: UserFinderService = new UserFinderService(req, res);
    userFinderService.getUser(userId);
};

export {getParamRequest};