import {Request, Response} from "express";
import {UserChangerService} from "../../../../services/user/user-changer.service";

const patchRequest = (req: Request, res: Response): void => {
    const userId: string = req.params.userId;
    const mail: string = req.body.mail;

    const userChangerService: UserChangerService = new UserChangerService(req, res);
    userChangerService.changeMail(userId, mail);
};

export {patchRequest};