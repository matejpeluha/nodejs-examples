import {Router} from "express";
import {getRequest} from "./get.request";
import {getParamRequest} from "./get.param.request";
import { deleteParamRequest } from "./delete.param.request";
import {mail} from "./mail";


const users: Router = Router();

users.use("/:userId/mail", mail);

users.get("/", getRequest);
users.get("/:userId", getParamRequest);
users.delete("/:userId", deleteParamRequest);

export {users};