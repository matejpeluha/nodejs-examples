import {Request, Response} from "express";
import {UserFinderService} from "../../../services/user/user-finder.service";


const getRequest = (req: Request, res: Response): void => {
    const userFinderService: UserFinderService = new UserFinderService(req, res);
    userFinderService.getAllUsers();
};

export {getRequest};