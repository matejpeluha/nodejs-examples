import {Router} from "express";
import {postRequest} from "./post.request";

const registration: Router = Router();

registration.post("/", postRequest);

export {registration};