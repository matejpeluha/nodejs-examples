import {Request, Response} from "express";
import UserRegistrationService from "../../../services/user/user-registration.service";


const postRequest = (req: Request, res: Response): void => {
    const userRegistrationService: UserRegistrationService = new UserRegistrationService(req, res);
    userRegistrationService.registerUser();
};

export {postRequest};