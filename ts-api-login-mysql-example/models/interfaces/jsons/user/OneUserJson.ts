interface OneUserJson{
    ID: number,
    first_name: string,
    last_name: string,
}

export {OneUserJson};