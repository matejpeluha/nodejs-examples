import {OneProfileJson} from "./OneProfileJson";

interface OneFullProfileJson extends OneProfileJson{
    password: string
}

export {OneFullProfileJson};