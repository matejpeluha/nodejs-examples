interface OneUserProfileJson{
    first_name: string,
    last_name: string,
    userId: number,
    mail: string
}

export {OneUserProfileJson};