
import {OneUserProfileJson} from "./OneUserProfileJson";

interface AllUserProfilesJson{
    allUsers: OneUserProfileJson[]
}

export {AllUserProfilesJson};