interface CookieSessionOptions{
    httpOnly: boolean;
    sameSite: boolean;
    maxAge: number;
}

export {CookieSessionOptions};