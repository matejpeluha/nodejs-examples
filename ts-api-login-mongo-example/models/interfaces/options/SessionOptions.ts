import {CookieSessionOptions} from "./CookieSessionOptions";

interface SessionOptions{
    secret: string;
    name: string;
    saveUninitialized: boolean;
    cookie: CookieSessionOptions;
    resave: boolean;
}

export {SessionOptions};