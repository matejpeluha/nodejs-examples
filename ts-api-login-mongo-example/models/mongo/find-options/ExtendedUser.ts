import {UserInterface} from "../schemas/User.schema";

interface ExtendedUser extends UserInterface{
    _id: number;
}

export {ExtendedUser};