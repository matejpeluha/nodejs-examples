import {Model, Schema} from "mongoose";
import {connection} from "../../../config/mongo.config";
import * as autoIncrement from "mongoose-auto-increment";


interface UserInterface{
    first_name: string;
    last_name: string;
    password: string;
    mail: string;
}

const UserSchema: Schema<UserInterface> = new Schema<UserInterface>({
    first_name: {type: String, required: true},
    last_name: {type: String, required: true},
    password: {type: String, required: true},
    mail: {type: String, required: true},
});

UserSchema.plugin(autoIncrement.plugin, 'User');

const UserModel: Model<UserInterface> = connection.model<UserInterface>('User', UserSchema);

export {UserModel, UserInterface};