import bodyParser from 'body-parser';
import cors from 'cors';
import express, {Application} from 'express';

import {loginSession} from './config/session.config';
import {apiRouter} from './router';


// deklaracia expressu a portu
const app: Application = express();
const port: number = 4000;

// CORS POLICIES
app.use(cors());

// KONFIGURACIA BODY PARSER MIDDLEWARU NA PARSOVANIE TELA REQUESTU
app.use(bodyParser.urlencoded({ extended: true }));
app.use(bodyParser.json());

// NASTAVENIE SESSION
app.use(loginSession);

/*
REQUESTY
 */
app.use(apiRouter);

/*
SPUSTENIE APPKY
 */
app.listen(port, () => {
    console.log('APP running on port ' + port);
});


