import {Router} from "express";
import {login} from "./login";
import {logout} from "./logout";
import {registration} from "./registration";
import {getRequest} from "./get.request";
import {users} from "./users";


const root: Router = Router();


root.use("/login", login);
root.use("/logout", logout);
root.use("/registration", registration);
root.use("/users", users);

root.get("/", getRequest);


export {root};