import {Router} from "express";
import {getRequest} from "./get.request";
import {patchRequest} from "./patch.request";

const mail: Router = Router({ mergeParams: true });

mail.get("/", getRequest);
mail.patch("/", patchRequest);

export {mail};