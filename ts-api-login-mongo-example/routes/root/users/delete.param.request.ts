import {Request, Response} from "express";
import {UserChangerService} from "../../../services/user/user-changer.service";

const deleteParamRequest = (req: Request, res: Response): void => {
    const userId: string = req.params.userId;
    const userChangerService: UserChangerService = new UserChangerService(req, res);
    userChangerService.deleterUser(userId);
};

export {deleteParamRequest};

