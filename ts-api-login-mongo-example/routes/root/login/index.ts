import {Router} from "express";
import {postRequest} from './post.request';

const login: Router = Router();

login.post("/", postRequest);

export {login};