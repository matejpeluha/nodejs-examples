import {Response} from "express";


abstract class ApiRequestService{
    protected res: Response;

    protected constructor(res: Response) {
        this.res = res;
    }
}

export {ApiRequestService};