import bcrypt from 'bcrypt';
import {Request, Response} from "express";
import {ApiRequestService} from "../abstract/api-request.service";
import {UserInterface, UserModel} from '../../models/mongo/schemas/User.schema';
import {EnforceDocument, CallbackError} from "mongoose";




class UserRegistrationService extends ApiRequestService{
    private readonly password: string;
    private readonly mail: string;
    private readonly firstName: string;
    private readonly lastName: string;



    constructor(req: Request, res: Response) {
        super(res);
        this.password = req.body.password;
        this.mail = req.body.mail;
        this.firstName = req.body.first_name;
        this.lastName = req.body.last_name;
    }

    public registerUser(): void{
        const saltRounds: number = 10;
        bcrypt.hash(this.password, saltRounds, this.saveToDatabase);
    }

    private saveToDatabase = (err: Error | undefined, hashedPassword: string): void => {
        const userOptions: UserInterface = {
            first_name: this.firstName,
            last_name: this.lastName,
            mail: this.mail,
            password: hashedPassword
        };
        const user: EnforceDocument<UserInterface, {}> =  new UserModel(userOptions);
        user.save(this.handleUserRegistration);
    }

    private handleUserRegistration = (err: CallbackError, user: UserInterface): void => {
        if (err){
           this.res.status(500).send(err);
        }
        else {
            this.res.sendStatus(200);
        }
    }
}

export default UserRegistrationService;