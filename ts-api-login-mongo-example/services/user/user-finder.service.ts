import {Request, Response} from "express";
import {ApiRequestService} from "../abstract/api-request.service";
import {UserModel} from "../../models/mongo/schemas/User.schema";
import {CallbackError} from "mongoose";
import {ExtendedUser} from "../../models/mongo/find-options/ExtendedUser";
import {LoginUserInfo} from "../../models/mongo/find-options/LoginUserInfo";

class UserFinderService extends ApiRequestService{

    constructor(req: Request, res: Response) {
        super(res);
    }

    public getAllUsers(): void{
        const filter: {} = {};

        UserModel.find(filter, this.handleGetAllUsers);
    }

    private handleGetAllUsers = (err: CallbackError, results: ExtendedUser[]): void => {
        if (err){
            this.res.status(500).send(err);
        }
        else{
            const jsonObject: {allUsers: ExtendedUser[]} = {allUsers: results};
            this.res.json(jsonObject);
        }
        this.res.end();
    }

    public getUser(userId: string): void{
        const filter: {_id: string} = {
            _id: userId
        };

        UserModel.findOne(filter, this.handleGetUser);
    }

    private handleGetUser = (err: CallbackError, user: ExtendedUser): void => {
        if (err){
            this.res.sendStatus(500);
        }
        else if (!user){
            this.res.status(404).send("User does not exist");
        }
        else{
            this.res.json(user);
        }

        this.res.end();
    }

    public getUserMail(userId: string): void{
        const filter = {
            _id: userId
        };

        UserModel.findOne(filter, this.handleGetUserMail);
    }

    private handleGetUserMail = (err: CallbackError, user: ExtendedUser): void => {
        if (err){
            this.res.sendStatus(500);
        }
        else if (!user){
            this.res.status(404).send("User does not exist");
        }
        else{
            const jsonObject: LoginUserInfo = {mail: user.mail};
            this.res.json(jsonObject);
        }

        this.res.end();
    }
}

export {UserFinderService};