import {Request, Response} from "express";
import {ApiRequestService} from "../abstract/api-request.service";
import {SimpleAnswerJson} from "../../models/interfaces/jsons/SimpleAnswerJson";
import {LoginUserInfo} from "../../models/mongo/find-options/LoginUserInfo";
import {UserModel} from "../../models/mongo/schemas/User.schema";
import {CallbackError} from "mongoose";


class UserChangerService extends ApiRequestService{

    constructor(req: Request, res: Response) {
        super(res);
    }

    public deleterUser(userId: string): void{
        const filter: {_id: string} = {
            _id: userId
        };

        const options: {} = {};


        UserModel.findOneAndDelete(filter, options, this.handleDeleteUser);
    }

    private handleDeleteUser = (err: CallbackError, user: any): void => {
        if (err){
            this.res.sendStatus(500);
        }
        else if (!user){
            this.res.status(404).send("User was not deleted, user does not exist");
        }
        else{
            const jsonObject: SimpleAnswerJson = {answer: "User deleted"};
            this.res.json(jsonObject);
        }
    }

    public changeMail(userId: string, mail: string): void{
        const filter: {_id: string} = {
            _id: userId
        };

        const update: LoginUserInfo = {
            mail: mail
        };

        const options: {new: boolean} = {
            new: true
        }

        UserModel.findOneAndUpdate(filter, update, options, this.handleChangeMail);
    }

    private handleChangeMail = (err: CallbackError, user: any): void => {
        if (err){
            this.res.sendStatus(500);
        }
        else if (!user){
            this.res.status(404).send("User does not exist");
        }
        else{
            const jsonObject: SimpleAnswerJson = {answer: "Mail updated"};
            this.res.json(jsonObject);
        }
        this.res.end();
    }
}

export {UserChangerService};