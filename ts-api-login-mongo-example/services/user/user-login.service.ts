import bcrypt from 'bcrypt';

import {Request, Response} from "express";
import {LoginSession} from "../../models/interfaces/sessions/LoginSession";
import {SimpleAnswerJson} from "../../models/interfaces/jsons/SimpleAnswerJson";
import {ApiRequestService} from "../abstract/api-request.service";
import {LoginUserInfo} from "../../models/mongo/find-options/LoginUserInfo";
import {UserModel} from "../../models/mongo/schemas/User.schema";
import {CallbackError} from "mongoose";
import {ExtendedUser} from "../../models/mongo/find-options/ExtendedUser";

class UserLoginService extends ApiRequestService{
    private readonly mail: string;
    private readonly password: string;
    private userId: number | undefined;
    private session: LoginSession;

    constructor(req: Request, res: Response) {
        super(res);
        this.mail = req.body.mail;
        this.password = req.body.password;
        this.session = (req.session as LoginSession);
    }

    public loginUser(): void{
        const filter: LoginUserInfo = {
            mail: this.mail
        };

        UserModel.findOne(filter, this.handleLoginUser);
    }

    private handleLoginUser = (err: CallbackError, user: ExtendedUser | undefined) => {
        if (err){
            this.res.status(500).send(err);
        }
        else if (!user){
            this.res.status(401).send("Non existing mail");
        }
        else{
            this.userId = user._id;
            const savedPassword: string = user.password;
            bcrypt.compare(this.password, savedPassword, this.handleLogin);
        }
    }

    private handleLogin = (err: Error | undefined, result: boolean | undefined): void => {
        if (err){
            this.res.status(500).send(err);
        }
        else if (!result){
            this.res.status(401).send("Wrong password");
        }
        else{
            this.session.userId = this.userId;
            this.res.status(200).send("Logged IN");
        }
        this.res.end();
    }

    public logoutUser(): void{
        this.session.destroy(() => {
            this.res.status(200).send("Logged OUT");
        });
    }

    public showLoggedInfo(): void{
        const userId: number | undefined = this.session.userId;
        if (userId){
            this.showLoggedName(userId);
        }
        else{
            const jsonObject: SimpleAnswerJson = {answer: "NOT LOGGED"};
            this.res.json(jsonObject);
        }
    }

    private showLoggedName(userId: number | undefined): void{
        const filter: {_id: number | undefined} = {
            _id: userId
        };

        UserModel.findOne(filter, this.handleShowLoggedName);
    }

    private handleShowLoggedName = (err: CallbackError, user: ExtendedUser | undefined): void => {
        if (err){
            this.res.status(500).send(err);
        }
        else if (!user){
            this.session.destroy(() => {
                this.res.status(401).send("Account was propably deleted or corupted");
            });
        }
        else{
            const userName: string = user.first_name + " " + user.last_name;
            const jsonObject: SimpleAnswerJson = {answer: userName + " Logged"};
            this.res.json(jsonObject);
        }
    }
}

export default UserLoginService;