import bcrypt from 'bcrypt';

import getConnection from "../config/mysql.config.js";

class UserLoginService{
    #connection;
    #mail;
    #password;
    #profileId;
    #res;
    #session;

    constructor(req, res) {
        this.#connection = getConnection();
        this.#mail = req.body.mail;
        this.#password = req.body.password;
        this.#res = res;
        this.#session = req.session;
    }

    loginUser(){
        const queryString = "SELECT * FROM profiles WHERE mail=?";
        const values = [this.#mail];

        this.#connection.query(queryString, values, this.#handleUserLogin);
    }

    #handleUserLogin = (err, results) =>{
        if(err){
            this.#res.statusCode(500);
        }
        else if(results.length < 1){
            this.#res.status(409).send("Profile does not exist");
        }
        else{
            this.#profileId = results[0].ID;
            const savedPassword = results[0].password;
            bcrypt.compare(this.#password, savedPassword, this.#handleLogin);
        }
    }

    #handleLogin = (err, result ) =>{
        if(err){
            this.#res.sendStatus(500);
        }
        else if(!result){
            this.#res.status(403).send("Wrong password");
        }
        else{
            this.#session.profileId = this.#profileId;
            this.#res.status(200).send("PRIHLASENY");
        }
        this.#res.end();
    }

    logoutUser(){
        this.#session.destroy();
        this.#res.status(200).send("ODHLASENY");
    }

    showLoggedInfo(){
        if(this.#session.profileId){
            this.#showLoggedName();
        }
        else{
            const jsonObject = {answer: "NOT LOGGED"};
            this.#res.json(jsonObject);
        }
    }

    #showLoggedName(){
        const queryString = "SELECT users.first_name, users.last_name FROM users " +
                            "JOIN profiles ON users.ID=profiles.user_id " +
                            "WHERE profiles.ID=?";
        const values = [this.#session.profileId];

        this.#connection.query(queryString, values, this.#handleShowLoggedName);
    }

    #handleShowLoggedName = (err, results) =>{
        let jsonObject;

        if(err){
            this.#res.sendStatus(500);
        }
        else{
            const userName = results[0].first_name + " " + results[0].last_name;
            const jsonObject = {answer: userName + " Logged"};
            this.#res.json(jsonObject);
        }
    }
}

export default UserLoginService;