import UserLoginService from "../services/user-login.service.js";

function setRootRoute(router){
    const path = "/";

    router.route(path).get(getRequest);
}


const getRequest = (req, res) =>{
    const userLoginService = new UserLoginService(req, res);
    userLoginService.showLoggedInfo();
}


export default setRootRoute;