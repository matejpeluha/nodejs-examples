import UserLoginService from "../../services/user-login.service.js";

function setLogoutRoute(urlRouter){
    const path = "/logout";

    urlRouter.route(path).get(getRequest);

    return urlRouter;
}

const getRequest = (req, res) =>{
    const userLoginService = new UserLoginService(req, res);
    userLoginService.logoutUser();
}

export default setLogoutRoute;