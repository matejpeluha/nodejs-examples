import session from 'express-session';


function getSession(){
    const oneDay = 1000 * 60 * 60 * 24;

    const sessionOptions = {
        secret: "thisismysecrctekeyfhrgfgrfrty84fwir767",
        name: "network-log",
        saveUninitialized: true,
        cookie: {
            httpOnly: true,
            sameSite: true,
            maxAge: oneDay,
        },
        resave: false
    }

    return session(sessionOptions);
}


export default getSession;