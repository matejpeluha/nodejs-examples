import {Session} from "express-session";

interface LoginSession extends Session{
    profileId: number | undefined
}

export {LoginSession};