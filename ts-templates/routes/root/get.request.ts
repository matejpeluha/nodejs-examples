import {Request, Response} from "express";
import UserLoginService from "../../services/user/user-login.service";

const getRequest = (req: Request, res: Response): void =>{
    const userLoginService: UserLoginService = new UserLoginService(req, res);
    userLoginService.showLoggedInfo();
}

export {getRequest};