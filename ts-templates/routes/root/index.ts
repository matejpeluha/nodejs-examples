import {Router} from "express";
import {getRequest} from "./get.request";


const root: Router = Router();


root.get("/", getRequest);


export {root};